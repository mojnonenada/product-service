package ru.MPEI.productservice;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.testcontainers.containers.MongoDBContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;
import ru.MPEI.productservice.dtos.ProductRequest;
import ru.MPEI.productservice.models.Product;
import ru.MPEI.productservice.repositories.ProductRepository;

import java.math.BigDecimal;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@Testcontainers
@AutoConfigureMockMvc
class ProductServiceApplicationTests {

	@Container
	static MongoDBContainer mongoDBContainer = new MongoDBContainer("mongo:4.4.2");

	@Autowired
	private MockMvc mockMvc;
	@Autowired
	private ObjectMapper objectMapper;
	private static final String PRODUCT_API_URL = "/api/product";
	@Autowired
	private ProductRepository productRepository;

	@DynamicPropertySource
	static void setProperties(DynamicPropertyRegistry dynamicPropertyRegistry) {
		dynamicPropertyRegistry.add("spring.data.mongodb.uri", mongoDBContainer::getReplicaSetUrl);
	}

	@Test
	void shouldCreateProduct() throws Exception {
		ProductRequest productRequest = getProductRequest();
		String productRequestString = objectMapper.writeValueAsString(productRequest);
		mockMvc.perform(MockMvcRequestBuilders.post(PRODUCT_API_URL)
					.contentType(MediaType.APPLICATION_JSON)
					.content(productRequestString))
				.andExpect(status().isCreated());

        Assertions.assertEquals(1, productRepository.findAll().size());
	}

	@Test
	void shouldGetAllProducts() throws Exception {
		productRepository.deleteAll();

		Product product1 = Product.builder()
				.name("Product 1")
				.description("Product 1 description")
				.price(BigDecimal.valueOf(100))
				.build();

		Product product2 = Product.builder()
				.name("Product 2")
				.description("Product 2 description")
				.price(BigDecimal.valueOf(200))
				.build();

		productRepository.save(product1);
		productRepository.save(product2);

		mockMvc.perform(MockMvcRequestBuilders.get(PRODUCT_API_URL)
						.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$[0].name").value("Product 1"))
				.andExpect(jsonPath("$[0].description").value("Product 1 description"))
				.andExpect(jsonPath("$[0].price").value(100))
				.andExpect(jsonPath("$[1].name").value("Product 2"))
				.andExpect(jsonPath("$[1].description").value("Product 2 description"))
				.andExpect(jsonPath("$[1].price").value(200));
	}

	private ProductRequest getProductRequest() {
		return ProductRequest.builder()
				.name("Iphone 15")
				.description("Iphone 15 description")
				.price(BigDecimal.valueOf(1500))
				.build();
	}

}