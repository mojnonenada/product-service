package ru.MPEI.productservice.repositories;

import org.springframework.data.mongodb.repository.MongoRepository;
import ru.MPEI.productservice.models.Product;

public interface ProductRepository extends MongoRepository<Product, String> {

}
